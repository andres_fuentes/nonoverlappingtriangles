import sage.graphs as graphs
import numpy as np
import itertools
from sympy import Point, Triangle, Segment
import struct
import multiprocessing 
import os.path
from os import path

def all_triangles(n):
    ts =[]
    numbers = range(0,n)
    ts = list(itertools.combinations(numbers,3))
    return ts 

def are_adjacent(abc,pqr):    
    a,b,c = abc
    p,q,r = pqr
    #every point in common serves as pivot
    if a in (p,q,r): 
        if b in (p,q,r):
            return True
        elif c in (p,q,r):
            return True
        else:
            return False
    elif b in (p,q,r):
        if a in (p,q,r):
            return True
        elif c in (p,q,r):
            return True
        else:
            return False
    elif c in (p,q,r):
        if a in (p,q,r):
            return True
        elif b in (p,q,r):
            return True
        else:
            return False
    else:
        return False

def makeTriangle(points, ts):
    triangulos = []
    p1,p2,p3 = map(Point, [ points[ts[0]], points[ts[1]], points[ts[2]] ])
    triangle = Triangle(p1,p2,p3)
    if(isinstance(triangle,Segment)):
        print(points[ts[0]], points[ts[1]], points[ts[2]])
    return triangle

def triangleContain(ta,tb):
    sum = 0;
    for vertice in tb.vertices:
        if(ta.contains(vertice) or ta.encloses_point(vertice)):
            sum += 1
    if sum == 3:
        return True
    else:
        return False

def intersect(a, b, points):
    ta = makeTriangle(points, a)
    tb = makeTriangle(points, b)
    if not set(a).isdisjoint(b):
        intersection = ta.intersection(tb)
        if  len(intersection) == 1 and isinstance(intersection[0],Point):
            return triangleContain(ta,tb) or triangleContain(tb,ta)
    else:
        return False
    return True

def mktrianglegraph(n,points):
    ts = all_triangles(n)
    tsinv = {}
    for i in range(len(ts)):
        tsinv[ts[i]] = i
    es = []
    for i in range(len(ts)):
        for j in range(i+1,len(ts)):
            if are_adjacent(ts[i],ts[j]) or intersect(ts[i],ts[j],points):
                es.append((ts[i],ts[j]))
    return (ts,[(tsinv[t1],tsinv[t2]) for (t1,t2) in es])

def hasIndependentSetsOfSizeAtLeast(points):
    ts,es = mktrianglegraph(len(points), points)
    g = graphs.graph.Graph(es)
    #print(g.girth())
    #P = g.plot()
    #P.show()
    max
    #MIS = graphs.independent_sets.IndependentSets(g,maximal=True)
    maxLength = 0
    response = []
    for i in graphs.independent_sets.IndependentSets(g,maximal=True):
        if len(i) >= maxLength:
            response = i
            maxLength = len(i)
    return (response,ts)

def getConvexPolygon(n):
    return [(cos(t), sin(t)) for t in srange(0, 2*pi, 2*pi/n)]

def drawResult(points,triangulos, resultado):
    P = point(points)
    for r in resultado:
        triangulo = triangulos[r]
        P += polygon([points[triangulo[0]],points[triangulo[1]],points[triangulo[2]]], alpha=0.3)
    return P

def ordertypes(n):
    wsize = 2
    fmttype = "H"
    if n < 9: 
        fname = "otypes0%d.b08" %(n,)
        wsize = 1
        fmttype = "B"
    elif n == 9:
        fname = "otypes09.b16"
    else: 
        fname = "otypes%d.b16" % (n,)
    f = open(fname, "rb")
    content = f.read()
    totalints = len(content)//wsize
    ints = struct.unpack("<"+str(totalints)+fmttype, content)
    pointsets = []
    i = 0
    for set in range(totalints//(2*n)):
        pointset= []
        for idx in range(n):
            pointset.append((ints[i],ints[i+1]))
            i +=2
        pointsets.append(pointset)
    return pointsets

setNumber = 5
orders = ordertypes(setNumber)

def calculate_overlapping(index):
    name = 'Results_' + str(setNumber) + '/result_' + str(index) + '.txt'
    if not path.exists(name):
        points = orders[index]
        total = len(orders)
        resultado,triangulos = hasIndependentSetsOfSizeAtLeast(points)
        currentResult = [triangulos[x] for x in resultado]
        with open(name, 'w') as f:
            f.write("%s\n" % currentResult)
        
nprocs = multiprocessing.cpu_count()
pool = multiprocessing.Pool(processes=nprocs)

pool.map(calculate_overlapping, range(0, len(orders)))